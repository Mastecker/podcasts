# Podcasts

* [Kurs als Ebook](https://mastecker.gitlab.io/podcasts/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/podcasts/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/podcasts/index.html)
